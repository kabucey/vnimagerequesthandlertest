//
//  ImageRequestApp.swift
//  Shared
//
//  Created by Kelly Bucey on 11/23/20.
//

import SwiftUI

@main
struct ImageRequestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
